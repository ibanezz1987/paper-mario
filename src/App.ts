import { Game } from "./classes/game";

class App {
  constructor() {
    this.init();
  }

  init() {
    new Game();
  }
}
new App();
