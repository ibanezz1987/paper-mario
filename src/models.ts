import * as THREE from "three";

export enum mouse {
  left = 0,
  middle = 1,
  right = 2,
  rotate = 0,
  dolly = 1,
  pan = 2,
}

export enum CameraType {
  PerspectiveCamera,
  OrthographicCamera,
}

export interface MapData {
  tiles: (number | null)[][][];
}

export interface Material {
  default: THREE.MeshBasicMaterial;
  side1: THREE.MeshBasicMaterial;
  side2: THREE.MeshBasicMaterial;
}
