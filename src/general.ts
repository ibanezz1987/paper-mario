import { MapData } from "./models";

const n = null;

export const mapData: MapData = {
  tiles: [
    [
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, 0, 0, 0, n, n, n, n, n, n, n, n],
    ],
    [
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
    ],
    [
      [n, n, 0, n, n, n, n, n, n, n, n, 0, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
    ],
    [
      [n, 0, 0, 0, n, 0, n, n, n, n, 0, 0, 0, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
      [n, n, n, n, n, n, n, n, n, n, n, n, n, n],
    ],
    [
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
      [0, 0, n, n, 0, 0, n, n, n, n, 0, 0, 0, 0],
    ],
    [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, n, n, 0, 0, 0, 0, 0],
    ],
  ],
};
