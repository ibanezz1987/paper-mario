import * as THREE from "three";
import { MapData, Material } from "../models";
// import { Tile } from "./plane";

export class GameMap {
  private mapData!: MapData;
  // private group: THREE.Group;
  private tileSize = 100;
  private mapYChange = 135;

  constructor(private readonly scene: THREE.Scene) {
    this.onInit();
  }

  private onInit() {
    // this.addGroup();
  }

  public setMapData(mapData: MapData) {
    this.mapData = mapData;
    // this.clearTiles();
    // this.clearWater();
    this.addWater();
    this.addTiles();
  }

  // private addGroup() {
  //   // this.group = new THREE.Group();
  //   // this.scene.add(this.group);
  // }

  private addWater() {
    const material = new THREE.MeshBasicMaterial({
      color: 0x006fd2,
      transparent: true,
      opacity: 0.5,
    });
    const xTiles = this.mapData.tiles[0][0].length;
    const zTiles = this.mapData.tiles[0].length;
    const boxGeometry = new THREE.BoxGeometry(
      this.tileSize * xTiles - 2,
      this.tileSize * zTiles * 20,
      this.tileSize
    );
    const box = new THREE.Mesh(boxGeometry, [
      material,
      material,
      material,
      material,
      material,
    ]);
    box.rotateX(-Math.PI / 2);
    box.position.setX((this.tileSize * xTiles) / 2);
    box.position.setY(this.tileSize * 0.75 - 2 - this.mapYChange);
    box.position.setZ(this.tileSize * zTiles * 10);
    this.scene.add(box);
    // this.group.add(box);
  }

  private addTiles() {
    const materials: Material[] = [
      {
        default: new THREE.MeshBasicMaterial({
          color: 0x8fca85,
        }),
        side1: new THREE.MeshBasicMaterial({
          color: 0x96a76f,
        }),
        side2: new THREE.MeshBasicMaterial({
          color: 0xa79c6f,
        }),
      },
      {
        default: new THREE.MeshBasicMaterial({
          color: 0xcabf85,
        }),
        side1: new THREE.MeshBasicMaterial({
          color: 0x96a76f,
        }),
        side2: new THREE.MeshBasicMaterial({
          color: 0xa79c6f,
        }),
      },
    ];

    this.mapData.tiles.forEach((layer, layerIndex) => {
      const layerNumber = this.mapData.tiles.length - layerIndex - 1;
      layer.forEach((tilesRow, rowIndex) => {
        tilesRow.forEach((material, tileIndex) => {
          if (material !== null) {
            const boxGeometry = new THREE.BoxGeometry(
              this.tileSize,
              this.tileSize,
              this.tileSize
            );
            const box = new THREE.Mesh(boxGeometry, [
              materials[material].side1,
              materials[material].side1,
              materials[material].side2,
              materials[material].side2,
              materials[material].default,
            ]);
            box.rotateX(-Math.PI / 2);
            box.position.setX(this.tileSize * tileIndex + this.tileSize / 2);
            box.position.setY(
              this.tileSize * layerNumber + this.tileSize / 2 - this.mapYChange
            );
            box.position.setZ(this.tileSize * rowIndex + this.tileSize / 2);
            this.scene.add(box);
            // this.group.add(box);
          }
        });
      });
    });
  }
}
