export class Vector2 {
  x = 0;
  y = 0;

  constructor(x?: number, y?: number) {
    this.x = typeof x !== "undefined" ? x : 0;
    this.y = typeof y !== "undefined" ? y : 0;
  }

  multiplyWith(v: Vector2): Vector2 {
    if (v.constructor === Vector2) {
      this.x *= v.x;
      this.y *= v.y;
    }
    return this;
  }

  addTo(v: Vector2): Vector2 {
    if (v.constructor === Vector2) {
      this.x += v.x;
      this.y += v.y;
    }
    return this;
  }

  add(v: Vector2): Vector2 {
    var result = this.copy();
    return result.addTo(v);
  }

  multiply(v: Vector2): Vector2 {
    var result = this.copy();
    return result.multiplyWith(v);
  }

  copy(): Vector2 {
    return new Vector2(this.x, this.y);
  }

  distanceFrom(obj: Vector2): number {
    return Math.sqrt(
      (this.x - obj.x) * (this.x - obj.x) + (this.y - obj.y) * (this.y - obj.y)
    );
  }
}
