import * as THREE from "three";
import { Vector2 } from "three";
import { MapData } from "../models";

export class Player {
  sprite!: THREE.Sprite;
  isStanding = true;
  isLookingAhead = true;
  material!: {
    standingRight: THREE.SpriteMaterial;
    standingLeft: THREE.SpriteMaterial;
    movingRight: THREE.SpriteMaterial;
    movingLeft: THREE.SpriteMaterial;
  };
  tmpBottom = 100 + 33;
  position = new Vector2(100, 100 + 33);
  velocity = new Vector2(0, 0);
  maxSideVelocity = 10;
  minSideVelocity = 0.1;
  sideAcceleration = 2;
  sideFriction = 0.9;
  initialJumpVelocity = 20;
  gravity = -0.5;
  size = { width: 100, height: 134 };
  currentFrame = 0;

  constructor(private readonly scene: THREE.Scene) {
    this.onInit();
  }

  private onInit() {}

  create() {
    this.getSprites();
    this.updateSprite();
    this.sprite.scale.set(this.size.width, this.size.height, 1);

    // setInterval(() => {
    //   this.isStanding = !this.isStanding;
    //   this.updateSprite();
    // }, 500);
  }

  update() {
    // Update velocity X
    this.position.x = Math.round(this.position.x + this.velocity.x);
    this.velocity.x = this.velocity.x * this.sideFriction;

    if (
      this.velocity.x < this.minSideVelocity &&
      this.velocity.x > -this.minSideVelocity
    ) {
      this.velocity.x = 0;
      this.isStanding = true;
    }

    // Update velocity Y
    this.position.y = this.position.y + this.velocity.y;
    this.velocity.y = this.velocity.y + this.gravity;

    if (this.position.y < this.tmpBottom) {
      this.position.y = this.tmpBottom;
      this.velocity.y = 0;
    }

    this.currentFrame++;

    this.updateSprite();
  }

  velocityLeft() {
    if (!this.sprite) return;
    this.isLookingAhead = false;
    this.updateIsStanding();

    if (this.velocity.x > 0) {
      this.velocity.x = 0;
    }
    this.velocity.x = this.velocity.x * this.sideAcceleration - 2;

    if (this.velocity.x < -this.maxSideVelocity) {
      this.velocity.x = -this.maxSideVelocity;
    }
  }

  velocityRight() {
    if (!this.sprite) return;
    this.isLookingAhead = true;
    this.updateIsStanding();

    if (this.velocity.x < 0) {
      this.velocity.x = 0;
    }
    this.velocity.x = this.velocity.x * this.sideAcceleration + 2;

    if (this.velocity.x > this.maxSideVelocity) {
      this.velocity.x = this.maxSideVelocity;
    }
  }

  velocityUp() {
    if (!this.sprite) return;
    if (this.isTouchingGround()) {
      this.velocity.y = this.initialJumpVelocity;
    }
  }

  private isTouchingGround = () => this.position.y === this.tmpBottom;

  private updateIsStanding() {
    this.isStanding =
      this.currentFrame % 10 < 5 ? this.isStanding : !this.isStanding;
  }

  private updateSprite() {
    if (this.sprite) {
      this.scene.remove(this.sprite);
    }
    this.sprite = new THREE.Sprite(this.getSprite());
    this.sprite.position.setX(this.position.x);
    this.sprite.position.setY(this.position.y);
    this.sprite.position.setZ(250);
    this.sprite.scale.set(this.size.width * -1, this.size.height, 1);
    // this.sprite.scale.setX(this.size.width * -1);
    this.scene.add(this.sprite);
  }

  private getSprite() {
    if (!this.isTouchingGround() || !this.isStanding) {
      return this.isLookingAhead
        ? this.material.movingRight
        : this.material.movingLeft;
    }
    return this.isLookingAhead
      ? this.material.standingRight
      : this.material.standingLeft;
  }

  private getSprites() {
    const spriteStandingRight = "images/sprites/mario-standing-right.png";
    const spriteStandingLeft = "images/sprites/mario-standing-left.png";
    const spriteMovingRight = "images/sprites/mario-moving-right.png";
    const spriteMovingLeft = "images/sprites/mario-moving-left.png";
    const mapStandingRight = new THREE.TextureLoader().load(
      spriteStandingRight
    );
    const mapStandingLeft = new THREE.TextureLoader().load(spriteStandingLeft);
    const mapMovingRight = new THREE.TextureLoader().load(spriteMovingRight);
    const mapMovingLeft = new THREE.TextureLoader().load(spriteMovingLeft);

    this.material = {
      standingRight: this.getNewTexture(mapStandingRight),
      standingLeft: this.getNewTexture(mapStandingLeft),
      movingRight: this.getNewTexture(mapMovingRight),
      movingLeft: this.getNewTexture(mapMovingLeft),
    };
  }

  private getNewTexture(map: THREE.Texture) {
    const color = 0xffffff;
    return new THREE.SpriteMaterial({
      map,
      color,
    });
  }
}
