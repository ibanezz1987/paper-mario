import * as THREE from "three";
import { CameraType, mouse } from "../models";
import { GameMap } from "./map";
// @ts-ignore: Use of untyped library
import { OrbitControls } from "./../jsm/controls/OrbitControls.js";
// @ts-ignore: Use of untyped library
import { TrackballControls } from "./../jsm/controls/TrackballControls.js";
// @ts-ignore: Use of untyped library
import Stats from "./../jsm/libs/stats.module.js";
import { mapData } from "./../general";
import { Player } from "./player";
import { Goomba } from "./goomba";

export class Game {
  private scene!: THREE.Scene;
  private activeCamera!: CameraType;
  private orthographicCamera!: THREE.Camera;
  private perspectiveCamera!: THREE.Camera;
  private orbitControls: OrbitControls;
  private trackballControls: TrackballControls;
  private stats: Stats;
  private renderer!: THREE.WebGLRenderer;
  private gameMap!: GameMap;
  private player!: Player;
  private goomba!: Goomba;
  private pointer = new THREE.Vector2();
  private hasDoneIntersectTest = false;
  private currentKeyDowns: string[] = [];

  constructor() {
    this.onInit();
  }

  private onInit() {
    this.addThreejs();

    // this.addAxis();
    this.addOrthographicCamera();
    this.addOrthographicCameraControls();
    this.addPerspectiveCamera();
    this.addPerspectiveCameraControls();

    this.addStats();

    this.gameMap = new GameMap(this.scene);
    this.gameMap.setMapData(mapData);
    document.addEventListener(
      "mousemove",
      (event) => this.savePointerPosition(event),
      false
    );

    this.player = new Player(this.scene);
    this.player.create();
    this.goomba = new Goomba(this.scene);
    this.goomba.create();

    this.animate();
    document.addEventListener("keydown", (event) => this.onKeyDown(event));
    document.addEventListener("keyup", (event) => this.onKeyUp(event));
  }

  private addThreejs() {
    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({ alpha: true });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement);
  }

  private onKeyDown(event: KeyboardEvent) {
    if (this.currentKeyDowns.indexOf(event.code) === -1) {
      this.currentKeyDowns.push(event.code);
    }
  }

  private onKeyUp(event: KeyboardEvent) {
    if (this.currentKeyDowns.indexOf(event.code) !== -1) {
      this.currentKeyDowns = this.currentKeyDowns.filter(
        (keyDown) => keyDown !== event.code
      );
    }
  }

  private updateShortcuts() {
    if (this.currentKeyDowns.indexOf("KeyO") !== -1) {
      this.activeCamera = CameraType.OrthographicCamera;
    } else if (this.currentKeyDowns.indexOf("KeyP") !== -1) {
      this.activeCamera = CameraType.PerspectiveCamera;
    }
    if (this.currentKeyDowns.indexOf("KeyD") !== -1) {
      this.player.velocityRight();
    } else if (this.currentKeyDowns.indexOf("KeyA") !== -1) {
      this.player.velocityLeft();
    }
    if (this.currentKeyDowns.indexOf("Space") !== -1) {
      this.player.velocityUp();
    }
  }

  private addPerspectiveCamera() {
    let aspect = window.innerWidth / window.innerHeight;
    // TODO: wrong type of camera? Always fixed on center?
    this.perspectiveCamera = new THREE.PerspectiveCamera(
      50,
      0.5 * aspect,
      1,
      10000
    );
    this.perspectiveCamera.position.z = 2000;
  }

  private addOrthographicCamera() {
    const cameraZoom = 5000;
    this.orthographicCamera = new THREE.OrthographicCamera(
      window.innerWidth / -2,
      window.innerWidth / 2,
      window.innerHeight / 2,
      window.innerHeight / -2,
      0.1,
      10000
    );
    this.orthographicCamera.position.x = cameraZoom;
    this.orthographicCamera.position.y = cameraZoom;
    this.orthographicCamera.position.z = cameraZoom;
  }

  private updateCameras() {
    // this.perspectiveCamera.position.x = this.player.position.x;
    this.perspectiveCamera.position.y = this.player.position.y + 200;

    this.scene.position.x = -this.player.position.x;
  }

  private addOrthographicCameraControls() {
    const controls = new TrackballControls(
      this.orthographicCamera,
      this.renderer.domElement
    );

    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.1;
    controls.mouseButtons = {
      LEFT: mouse.pan,
      MIDDLE: mouse.dolly,
      RIGHT: mouse.rotate,
    };
    controls.noRotate = false;
    this.trackballControls = controls;
  }

  private addPerspectiveCameraControls() {
    const controls = new OrbitControls(
      this.perspectiveCamera,
      this.renderer.domElement
    );
    this.orbitControls = controls;
  }

  private getActiveCamera() {
    switch (this.activeCamera) {
      case CameraType.OrthographicCamera:
        return this.orthographicCamera;
      default:
        return this.perspectiveCamera;
    }
  }

  private addStats() {
    this.stats = new Stats();
    document.querySelector("body")?.appendChild(this.stats.dom);
  }

  private savePointerPosition(event: MouseEvent) {
    this.pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    this.pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;
  }

  private addAxis() {
    const axis = new THREE.AxesHelper(1000);
    this.scene.add(axis);
  }

  private animate() {
    requestAnimationFrame(() => this.animate());
    this.trackballControls.update();
    this.orbitControls.update();
    this.render();
    this.player.update();
    this.goomba.update();
    this.updateCameras();
    this.updateShortcuts();
    this.stats.update();
  }

  private render() {
    const camera = this.getActiveCamera();
    if (!this.hasDoneIntersectTest) {
      this.hasDoneIntersectTest = true;
      console.log("this.scene.children", this.scene.children);
    }
    this.renderer.render(this.scene, camera);
  }
}
